use <config.scad>
use <ecke.scad>
use <rand.scad>
use <mitte.scad>

l=1;
w=1;
length=300;
width=200;

module onebyone(length,width){
    makeecke();
    translate([0,width,0]) rotate([0,0,-90])makeecke();
    translate([length,0,0]) rotate([0,0,90]) makeecke();
    translate([length,width,0])rotate([0,0,180]) makeecke();
}
module twobyone(length,width){
    makeecke();
    translate([0,length,0]) rotate([0,0,-90])makeecke();
    translate([width*2,0,0]) rotate([0,0,90]) makeecke();
    translate([width*2,length,0])rotate([0,0,180]) makeecke();
    translate([width,0,0]) makerand();
    translate([width,length,0])rotate([0,0,180])makerand();
}
module twobytwo(length,width){
    makeecke();
    translate([0,width*2,0]) rotate([0,0,-90])makeecke();
    translate([length*2,0,0]) rotate([0,0,90]) makeecke();
    translate([length*2,width*2,0])rotate([0,0,180]) makeecke();
    translate([length,0,0]) makerand();
    translate([0,width,0]) rotate([0,0,-90]) makerand();
    translate([length*2,width,0])rotate([0,0,90])makerand();
    translate([length,width*2,0])rotate([0,0,180])makerand();
    translate([length,width,0]) makemitte();
}
projection(){
    if(l==1 && w==1) onebyone(length,width);
    if((l==2 && w==1)||(l==1 && w==2)) twobyone(length,width);
    if(l==2 && w==2) twobytwo(length,width);
}
