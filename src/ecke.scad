include <config.scad>
//$fn=50;
//
//overall_higth=12;
//inner_lenght=20;
//side_width=15;
//edge_radius=15;
//screw_size=5;
//screw_head_size=8;
//screw_head_over_ground=2.5;
//angle_factor=1.4;


module screwhole(){
    color(rands(0,1,3)) // inner edge
        cylinder(h=screw_head_over_ground,d=screw_size);
    color(rands(0,1,3)) // inner edge
        translate([0,0,screw_head_over_ground])
        cylinder(h=overall_higth-screw_head_over_ground,d=screw_head_size);
}
module makeecke(){
    union(){
        difference(){
            difference(){
                //Body
                difference(){
                    union(){
                        color(rands(0,1,3))
                            translate([edge_radius,edge_radius,0])
                            cylinder(h=overall_higth,r=edge_radius);
                        color(rands(0,1,3))
                            translate([edge_radius,0,0])
                            cube([inner_lenght,side_width,overall_higth]);
                        color(rands(0,1,3))
                            translate([0,edge_radius,0])
                            cube([side_width,inner_lenght,overall_higth]);
                    }
                    color(rands(0,1,3)) // inner edge
                        translate([side_width,side_width,0])
                        cube([inner_lenght,inner_lenght,overall_higth]);
                }
                translate([side_width/2,inner_lenght+edge_radius-screw_head_size,0])
                    screwhole();
                translate([inner_lenght+edge_radius-screw_head_size,side_width/2,0])
                    screwhole();
            }

            CubePoints = [
                [  overall_higth           , overall_higth            , 0               ], //0
                [ inner_lenght+edge_radius , overall_higth            , 0               ], //1
                [ inner_lenght+edge_radius , inner_lenght+edge_radius , 0               ], //2
                [  overall_higth           , inner_lenght+edge_radius , 0               ], //3
                [  0                       , 0                        , overall_higth   ], //4
                [ inner_lenght+edge_radius , 0                        , overall_higth   ], //5
                [ inner_lenght+edge_radius , inner_lenght+edge_radius , overall_higth   ], //6
                [  0                       , inner_lenght+edge_radius , overall_higth   ]]; //7

            CubeFaces = [
                [0,1,2,3],  // bottom
                [4,5,1,0],  // front
                [7,6,5,4],  // top
                [5,6,2,1],  // right
                [6,7,3,2],  // back
                [7,4,0,3]]; // left

            translate([edge_radius/angle_factor,edge_radius/angle_factor])
                polyhedron( CubePoints, CubeFaces );
        }
    }
}
makeecke();
