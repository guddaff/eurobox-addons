plate_thickness=1;
plate_length=50;
plate_width=15;
wall_hight=20;
wall_thickness=1;
center_radius=20;

module arm(){
    difference(){
        color(rands(0,1,3))
            translate([0,plate_length/2,0])
            cube([plate_width,plate_length,wall_hight],center=true);
        union(){
            color(rands(0,1,3))
                translate([0,plate_length/2,wall_hight/4+plate_thickness/2])
                cube([plate_width-wall_thickness*2,plate_length,wall_hight/2],center=true);
            translate([0,plate_length/2,-wall_hight/4-plate_thickness/2])
                cube([plate_width-wall_thickness*2,plate_length,wall_hight/2],center=true);
        }
    }
}
module makestacker(){
    difference(){
        union(){
            arm();
            rotate([0,0,-90])
                arm();
        }
        union(){
            translate([0,0,-wall_hight/4-plate_thickness/2])
                color(rands(0,1,3))
                cube([center_radius*2,center_radius*2,wall_hight/2],center=true);
            translate([0,0,+wall_hight/4+plate_thickness/2])
                color(rands(0,1,3))
                cube([center_radius*2,center_radius*2,wall_hight/2],center=true);
            color(rands(0,1,3))
                rotate([0,0,45])
                translate([-center_radius,0,0])
                cube([center_radius*2,center_radius*2,plate_thickness],center=true);

        }
    }
}
makestacker();

