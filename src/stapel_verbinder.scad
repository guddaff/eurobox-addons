$fn=200;

module stack(){
    cylinder(h=6,d1=13,d2=10);
    translate([0,0,6])
        cylinder(h=3,d=10);translate([0,0,9])
        cylinder(h=1,d1=10,d2=8);
}
module makestacker(){
    stack();
    mirror([0,0,1])
        stack();
}
makestacker();
