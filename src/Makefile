OUTPUTFORMAT = stl
FOOTPRINTFORMAT = dxf

SRCDIR    = src
BUILDDIR  = stl
PICDIR	  = pic
FOOTDIR   = footprints

all: directories \
	$(BUILDDIR)/rand.$(OUTPUTFORMAT) \
	$(BUILDDIR)/mitte.$(OUTPUTFORMAT) \
	$(BUILDDIR)/ecke.$(OUTPUTFORMAT) \
	$(BUILDDIR)/stop.$(OUTPUTFORMAT) \
	$(BUILDDIR)/stapel_verbinder.$(OUTPUTFORMAT)\
	$(BUILDDIR)/stacker.$(OUTPUTFORMAT)

pictures: all \
	$(PICDIR)/rand.png \
	$(PICDIR)/mitte.png \
	$(PICDIR)/ecke.png \
	$(PICDIR)/stop.png \
	$(PICDIR)/stapel_verbinder.png\
	$(PICDIR)/stacker.png

footprint: all \
	$(FOOTDIR)/1x1_200x150.$(FOOTPRINTFORMAT) \
	$(FOOTDIR)/2x1_200x150.$(FOOTPRINTFORMAT) \
	$(FOOTDIR)/2x2_200x150.$(FOOTPRINTFORMAT) \
	$(FOOTDIR)/1x1_300x200.$(FOOTPRINTFORMAT) \
	$(FOOTDIR)/2x1_300x200.$(FOOTPRINTFORMAT) \
	$(FOOTDIR)/2x2_300x200.$(FOOTPRINTFORMAT) \
	$(FOOTDIR)/1x1_400x300.$(FOOTPRINTFORMAT) \
	$(FOOTDIR)/2x1_400x300.$(FOOTPRINTFORMAT) \
	$(FOOTDIR)/2x2_400x300.$(FOOTPRINTFORMAT) \
	$(FOOTDIR)/1x1_600x400.$(FOOTPRINTFORMAT) \
	$(FOOTDIR)/2x1_600x400.$(FOOTPRINTFORMAT) \
	$(FOOTDIR)/2x2_600x400.$(FOOTPRINTFORMAT) \
	$(FOOTDIR)/1x1_800x600.$(FOOTPRINTFORMAT) \
	$(FOOTDIR)/2x1_800x600.$(FOOTPRINTFORMAT) \
	$(FOOTDIR)/2x2_800x600.$(FOOTPRINTFORMAT)

everything: all pictures footprint

directories:
	@mkdir -p $(BUILDDIR) $(SRCDIR) $(PICDIR) $(FOOTDIR)

clean:
	@rm -rf $(BUILDDIR)/* $(FOOTDIR)/*

cleanall:
	@rm -rf $(BUILDDIR) $(PICDIR) $(FOOTDIR)

$(BUILDDIR)/%.$(OUTPUTFORMAT): $(SRCDIR)/%.scad
	openscad -o $@ $<

$(PICDIR)/%.png: $(SRCDIR)/%.scad
	openscad -o $@ --camera=0,0,0,60,0,135,0 --viewall --render $<

$(FOOTDIR)/1x1_200x150.$(FOOTPRINTFORMAT):
	openscad -D l=1 -D w=1 -D length=200 -D width=150 -o $(FOOTDIR)/1x1_200x150.$(FOOTPRINTFORMAT) --render $(SRCDIR)/footprint.scad
$(FOOTDIR)/2x1_200x150.$(FOOTPRINTFORMAT):
	openscad -D l=1 -D w=2 -D length=200 -D width=150 -o $(FOOTDIR)/2x1_200x150.$(FOOTPRINTFORMAT) --render $(SRCDIR)/footprint.scad
$(FOOTDIR)/2x2_200x150.$(FOOTPRINTFORMAT):
	openscad -D l=2 -D w=2 -D length=200 -D width=150 -o $(FOOTDIR)/2x2_200x150.$(FOOTPRINTFORMAT) --render $(SRCDIR)/footprint.scad
$(FOOTDIR)/1x1_300x200.$(FOOTPRINTFORMAT):
	openscad -D l=1 -D w=1 -D length=300 -D width=200 -o $(FOOTDIR)/1x1_300x200.$(FOOTPRINTFORMAT) --render $(SRCDIR)/footprint.scad
$(FOOTDIR)/2x1_300x200.$(FOOTPRINTFORMAT):
	openscad -D l=1 -D w=2 -D length=300 -D width=200 -o $(FOOTDIR)/2x1_300x200.$(FOOTPRINTFORMAT) --render $(SRCDIR)/footprint.scad
$(FOOTDIR)/2x2_300x200.$(FOOTPRINTFORMAT):
	openscad -D l=2 -D w=2 -D length=300 -D width=200 -o $(FOOTDIR)/2x2_300x200.$(FOOTPRINTFORMAT) --render $(SRCDIR)/footprint.scad
$(FOOTDIR)/1x1_400x300.$(FOOTPRINTFORMAT):
	openscad -D l=1 -D w=1 -D length=400 -D width=300 -o $(FOOTDIR)/1x1_400x300.$(FOOTPRINTFORMAT) --render $(SRCDIR)/footprint.scad
$(FOOTDIR)/2x1_400x300.$(FOOTPRINTFORMAT):
	openscad -D l=1 -D w=2 -D length=400 -D width=300 -o $(FOOTDIR)/2x1_400x300.$(FOOTPRINTFORMAT) --render $(SRCDIR)/footprint.scad
$(FOOTDIR)/2x2_400x300.$(FOOTPRINTFORMAT):
	openscad -D l=2 -D w=2 -D length=400 -D width=300 -o $(FOOTDIR)/2x2_400x300.$(FOOTPRINTFORMAT) --render $(SRCDIR)/footprint.scad
$(FOOTDIR)/1x1_600x400.$(FOOTPRINTFORMAT):
	openscad -D l=1 -D w=1 -D length=600 -D width=400 -o $(FOOTDIR)/1x1_600x400.$(FOOTPRINTFORMAT) --render $(SRCDIR)/footprint.scad
$(FOOTDIR)/2x1_600x400.$(FOOTPRINTFORMAT):
	openscad -D l=1 -D w=2 -D length=600 -D width=400 -o $(FOOTDIR)/2x1_600x400.$(FOOTPRINTFORMAT) --render $(SRCDIR)/footprint.scad
$(FOOTDIR)/2x2_600x400.$(FOOTPRINTFORMAT):
	openscad -D l=2 -D w=2 -D length=600 -D width=400 -o $(FOOTDIR)/2x2_600x400.$(FOOTPRINTFORMAT) --render $(SRCDIR)/footprint.scad
$(FOOTDIR)/1x1_800x600.$(FOOTPRINTFORMAT):
	openscad -D l=1 -D w=1 -D length=800 -D width=600 -o $(FOOTDIR)/1x1_800x600.$(FOOTPRINTFORMAT) --render $(SRCDIR)/footprint.scad
$(FOOTDIR)/2x1_800x600.$(FOOTPRINTFORMAT):
	openscad -D l=1 -D w=2 -D length=800 -D width=600 -o $(FOOTDIR)/2x1_800x600.$(FOOTPRINTFORMAT) --render $(SRCDIR)/footprint.scad
$(FOOTDIR)/2x2_800x600.$(FOOTPRINTFORMAT):
	openscad -D l=2 -D w=2 -D length=800 -D width=600 -o $(FOOTDIR)/2x2_800x600.$(FOOTPRINTFORMAT) --render $(SRCDIR)/footprint.scad
