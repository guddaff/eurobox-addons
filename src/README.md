# Eurobox-addons

This is a collection of addons designed for use with Euroboxes.
the models will likley be abel to be used with other sorts of boxes.
otherwise you can adjust the parameters in `src/config.scad`to fit your style of box.

## build

to build these models you need to run `make`.
after that you can find the stlmodels in `./stl/`

## pictures

![](pic/ecke.png)

![](pic/mitte.png)

![](pic/rand.png)

![](pic/stop.png)

![](pic/stapel_verbinder.png)

![](pic/stacker.png)
